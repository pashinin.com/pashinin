pashinin.com
============


Development
-----------

.. code-block:: bash

   yarn          # install dependencies
   yarn serve    # start dev-server (UI), open browser
   yarn dev-ssr  # watch changes for SSR
   yarn run-server


Additional commands:

Analyze bundle size with `yarn build --anal`.

Deployment
----------

Static files *must* be served by Nginx. While server-side rendering is
done by Node's :code:`express` server.

..
   The question is how to create a Docker image with 2 services running:
   Nginx for static requests and Node express backend for SSR.
