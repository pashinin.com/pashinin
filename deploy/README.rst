Deployment
==========

Can I use one single Nomad job but with canary_tags?
----------------------------------------------------

No. Example: rolling back "master" deployment will also roll back "stable"
version if they are both in the same file.
