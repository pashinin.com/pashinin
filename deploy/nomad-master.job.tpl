# -*- mode: hcl -*-
job "pashinin.com-master" {
  datacenters = ["moscow"]
  type = "service"

  constraint {
    attribute = "${meta.instance_type}"
    value = "worker"
  }

  # Openresty works only on modern CPU with SSE4.2
  constraint {
    attribute = "${attr.cpu.numcores}"
    operator  = ">="
    value     = "4"
  }

  group "app" {
    count = 1

    task "nginx" {
      driver = "docker"
      config {
        image = "registry.gitlab.com/pashinin.com/pashinin:master"
        force_pull = true
        port_map { http = 80 }
        volumes = [
          "local/nginx.conf:/usr/local/openresty/nginx/conf/nginx.conf",
        ]
      }

      template {
        data = <<EOH

EOH
        destination = "local/nginx.conf"
      }

      env {
        VERSION = "${TAG}-${CI_COMMIT_SHORT_SHA}"
        DATE = "${DATE}"
      }

      service {
        name = "pashinincom-staging"
        port = "http"

        tags = [
          # Fabio tags:
          "urlprefix-canary.pashinin.com:443/ allow=ip:10.254.239.0/24",
          "urlprefix-canary.pashinin.com:80/ redirect=301,https://canary.pashinin.com$path",
          # "urlprefix-:465 proto=tcp pxyproto=true",

          # Traefik tags:
          "traefik.enable=true",
          "traefik.frontend.entryPoints=https",
          "traefik.frontend.rule=Host:canary.pashinin.com",
          "traefik.tags=service",
        ]
        check {
          type = "http"
          path = "/courses"
          interval = "15s"
          timeout  = "2s"
        }
      } # service

      resources {
        cpu    = 100 # 100 = 100 MHz
        memory = 10  # actually takes 1 Mb, Nomad minimum is 10
        network {
          mbits = 10
          port "http" { }
          # port "https" { }
        }
      }
    } # task

    task "ssr" {
      driver = "docker"
      config {
        image = "registry.gitlab.com/pashinin.com/pashinin:master-ssr"
        force_pull = true
        port_map { http = 8080 }
      }

      env {
        VERSION = "${TAG}-${CI_COMMIT_SHORT_SHA}"
        DATE = "${DATE}"
      }

      service {
        name = "pashinincom-staging-ssr"
        port = "http"

        check {
          type = "http"
          path = "/courses"
          interval = "15s"
          timeout  = "2s"
        }
      } # service

      resources {
        cpu    = 500 # 100 = 100 MHz
        memory = 128  # actually takes ~70 Mb
        network {
          mbits = 10
          port "http" { }
          # port "https" { }
        }
      }
    } # task

  } # group
}
