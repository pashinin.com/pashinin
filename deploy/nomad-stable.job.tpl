# -*- mode: hcl -*-
job "pashinin.com-stable" {
  datacenters = ["moscow"]
  type = "service"

  constraint {
    attribute = "${meta.instance_type}"
    value = "worker"
    # Openresty works only on SSE4.2 CPU!
  }

  update {
    stagger = "30s"
    max_parallel = 1
    auto_revert = true
    # canary = 2
  }

  group "app" {
    count = 2

    restart {
      # mode = "delay"  # fail, delay (default)
      interval = "30s"
      attempts = 2
      delay = "10s"  # minimum wait
    }

    task "nginx" {
      driver = "docker"
      config {
        image = "registry.gitlab.com/pashinin.com/pashinin:v1.2.0"

        # if BRANCH is rolling ("stable") and need to redownload it
        force_pull = true

        # Openresty runs only on CPU with SSE 4.2
        # https://github.com/openresty/docker-openresty/issues/39
        port_map { http = 80 }

        # https://github.com/openresty/docker-openresty/blob/master/nginx.conf
        volumes = [
          "local/default.conf:/usr/local/openresty/nginx/conf/nginx.conf",
        ]
      }

      template {
        data = <<EOH

EOH
        destination = "local/default.conf"
      }

      env {
        VERSION = "${BRANCH}-${CI_COMMIT_SHORT_SHA}"
        DATE = "${DATE}"
      }

      resources {
        cpu    = 100 # 100 = 100 MHz
        memory = 10 # 128 = 128 MB
        network {
          mbits = 10
          port "http" { }
          # port "https" { }
        }
      }

      service {
        name = "pashinincom"
        port = "http"
        tags = [
          # Fabio tags:
          "urlprefix-pashinin.com:443/",
          "urlprefix-pashinin.com:80/ redirect=301,https://pashinin.com$path",

          # Traefik tags:
          # "traefik.enable=true",
          # "traefik.frontend.entryPoints=https",
          # "traefik.frontend.rule=Host:pashinin.com",
          # "traefik.tags=service",
        ]
        check {
          type = "http"
          path = "/courses"
          interval = "20s"
          timeout  = "2s"
        }
      }
    } # task

    task "ssr" {
      driver = "docker"
      config {
        image = "registry.gitlab.com/pashinin.com/pashinin:v1.2.0-ssr"
        force_pull = true
        port_map { http = 8080 }
      }

      env {
        VERSION = "${TAG}-${CI_COMMIT_SHORT_SHA}"
        DATE = "${DATE}"
      }

      resources {
        cpu    = 500 # 100 = 100 MHz
        memory = 128  # actually takes ~70 Mb
        network {
          # mbits = 10
          port "http" { }
        }
      }

      service {
        name = "pashinincom-ssr"
        port = "http"
        # check {
        #   type = "http"
        #   path = "/courses"
        #   interval = "15s"
        #   timeout  = "2s"
        # }
      } # service
    } # task
  } # group end
}
