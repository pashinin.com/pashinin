SRC=~/src
DIR=$SRC/pashinin
linkto=$DIR/node_modules/commonfe

if [ -L ${linkto} ] ; then
    echo "link exists"
elif [ -e ${linkto} ] ; then
    echo "Not a link. Moved to '-stable' and created a link to dev version."
    mv ${linkto} ${linkto}-stable
    ln -s ${SRC}/commonfe ${linkto}
else
    ln -s ${SRC}/commonfe ${linkto}
fi;
