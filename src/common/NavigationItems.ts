import {
  // mdiMenu,
  mdiHome,
  mdiCurrencyUsd,
  // mdiChevronLeft,
  // mdiChevronRight,
  mdiFrequentlyAskedQuestions,
  mdiContacts,
} from '@mdi/js';

export default [
  // {
  //   to: '/',
  //   // title: 'Услуги и курсы',
  //   icon: mdiHome,
  // },
  {
    to: '/courses',
    title: 'Курсы',
    // icon: mdiHome,
  },
  {
    to: '/price',
    title: 'Оплата',
    // icon: mdiCurrencyUsd,
  },
  // {
  //   to: '/pay',
  //   title: 'Как оплатить',
  //   icon: 'fa-credit-card',
  // },
  {
    to: '/faq',
    title: 'Вопросы',
    // icon: mdiFrequentlyAskedQuestions,
  },
  {
    to: '/contacts',
    title: 'Контакты',
    // icon: mdiContacts,
  },
];
