/**
 * @fileOverview Basic API: making queries, etc...
 * @name api.ts:components
 * @author Sergey Pashinin
 * @license GPL3
 */

import axios from 'axios';
import Cookies from 'js-cookie';
// import { createUploadLink } from 'apollo-upload-client';

// const link = createUploadLink(/* Options */);

export default class API {

  public options: object;
  public auth: {
    type: {
      jwt: boolean;
    },
  };
  public axios: any;
  public jwt: boolean;
  public onLocalhost: boolean;
  public token: string;

  constructor(options) {
    // console.log(link);
    this.auth = { type: { jwt: true } };
    this.jwt = true;
    this.token = '';

    this.axios = axios.create({
      baseURL: 'https://api.pashinin.com/',
      timeout: 3000,
      // headers: {
      //   // 'X-Custom-Header': 'foobar'
      //   'X-CSRFToken': Cookies.get('csrftoken'),
      // },
    });
    this.options = { ...options };
  }

  /**
   * Returns axios instance and headers to be sent for current user.
   * @returns {axios, headers}
   */
  public prepare(): { ax: { get: any, post: any }, headers: object } {
    const headers = {
      Authorization: '',
    };
    if (this.auth.type.jwt) {
      // this.token = $store.state.user.token;
      const token = Cookies.get('access');
      headers.Authorization = `Bearer ${token}`;
    } else {
      headers['X-CSRFToken'] = Cookies.get('csrftoken');
    }
    return {
      ax: this.axios,
      headers,
    };
  }

  public get(url) {
    // this.axios.defaults.headers.common.Authorization = `JWT ${this.token}`;
    const { ax, headers } = this.prepare();
    // ax.post('/', {
    //   // graphql
    //   query: '{ allTasks { id } }',
    //   variable: null,
    //   operationName: null,
    // , { headers })
    // .then((response) => cb([123, response, this]))
    // .then(() => {
    // console.log(response);
    // this.$store.commit('LOGIN_SUCCESS', response);
    // })
    // .catch((error) => {
    // console.log(error);
    // .catch(() => cb([1, 2, 3]));
    // return [this];
    return ax.get(url, { headers });
  }

  public post(url, data) {
    // this.axios.defaults.headers.common.Authorization = `JWT ${this.token}`;
    const { ax, headers } = this.prepare();
    return ax.post(url, data, { headers });
  }


  public signin(email: string, password: string) {
    const query = `mutation {
  signin(email: "${email}", password: "${password}") {
    email
    jwt
  }
}`;
    return this.graphql({ query });
  }

  /**
   * Query GraphQL at api.domain.com/graphql
   */
  public graphql(options) {
    // this.axios.defaults.headers.common.Authorization = `JWT ${this.token}`;
    const { ax, headers } = this.prepare();
    return ax.post(
      '/graphql',
      // {
      //   // graphql
      //   query, // : '{ allTasks { id } }',
      //   variable: null,
      //   operationName: null,
      // },
      options,
      { headers },
    );
  }
}
