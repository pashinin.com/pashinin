import Vue from 'vue';
import App from './App.vue';
import store from '@/store';
import './registerServiceWorker';
import { createRouter } from './router';

import Meta from 'vue-meta';
Vue.use(Meta);

import VueGtag from 'vue-gtag';
Vue.use(VueGtag, {
  config: { id: 'UA-37941248-1' },
  includes: [
    { id: 'AW-862093969' },
  ],
});

import {
  mdiAlert,
  mdiInformation,
  mdiCircle,
  mdiChevronLeft,
  mdiChevronRight,
  mdiClose,
  mdiCloseCircle,
} from '@mdi/js';


import Vuetify from 'vuetify/lib';
// import colors from 'vuetify/lib/util/colors';
// Scroll is not imported when using a-la-carte in Vuetify.
// So importing it manually.
import { Scroll } from 'vuetify/lib/directives';
Vue.use(Vuetify, {
  components: {
    // VDialog,
  },
  directives: {
    Scroll,
  },
});

Vue.config.productionTip = false;

// Has problem with SSR:
// https://github.com/vchaptsev/vue-yandex-metrika/issues/19
//
// import VueYandexMetrika from 'vue-yandex-metrika';
// Vue.use(VueYandexMetrika, {
//   id: 35428125,
//   router,
//   env: process.env.NODE_ENV,
//   // other options
// });

export function createApp() {
  const router = createRouter();
  const app = new Vue({
    store,
    render: (h) => h(App),
    router,
    vuetify: new Vuetify({
      directives: {
        Scroll,
      },
      theme: {
        options: {
          minifyTheme: (css) => {
            return process.env.NODE_ENV === 'production'
              ? css.replace(/[\r\n|\r|\n]/g, '')
              : css;
          },
        },
      },
      icons: {
        iconfont: 'mdi', // 'mdi' is default (only for display purposes)
        values: {
          // cancel: 'fas fa-ban',
          // menu: 'fas fa-ellipsis-v',
          info: mdiInformation,
          warning: mdiAlert,
          prev: mdiChevronLeft,
          next: mdiChevronRight,
          delimiter: mdiCircle,  // for carousel
          close: mdiClose,
          cancel: mdiCloseCircle,  // for alert "close" button
        },
      },
    }),
  });
  return { app, router };
}
