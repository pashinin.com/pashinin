/* tslint:disable:no-console */

import { register } from 'register-service-worker';
import store from '@/store/index';

if (process.env.NODE_ENV === 'production' && (typeof document !== 'undefined')) {
  register(`${process.env.BASE_URL}service-worker.js`, {
    ready() {
      console.log(`${process.env.NODE_ENV}`);
      console.log(`${process.env.BASE_URL}`);
      console.log(
        'App is being served from cache by a service worker.\n' +
        'For more details, visit https://goo.gl/AFskqB',
      );
    },
    cached() {
      console.log('Content has been cached for offline use.');
    },
    updated(reg) {
      console.log('New content is available; please refresh.');
      store.commit('setNewUpdate', reg.waiting);
    },
    offline() {
      // Test with:
      //   Firefox: File -> Work Offline
      console.log('No internet connection found. App is running in offline mode.');
    },
    error(error) {
      console.error('Error during service worker registration:', error);
    },
  });

  // Reload once when the new Service Worker is activated.
  // Event fires after skipWaiting() is done.
  //
  // "navigator.serviceWorker" can be undefined here.
  // It is ServiceWorkerContainer.
  let refreshing: boolean;
  if (navigator.serviceWorker) {
    navigator.serviceWorker.addEventListener('controllerchange', () => {
      if (refreshing) {
        return;
      }
      refreshing = true;
      window.location.reload();
    });
  }
}
