import Vue from 'vue';
import Router from 'vue-router';
import { RouterOptions, RouteConfig } from 'vue-router';
import Meta from 'vue-meta';

Vue.use(Router);
Vue.use(Meta);

function Component(name: string) {
  return () => import(`./views/${name}.vue`);
}

function View(path: string, component: string, name?: string) {
  if (name) {
    return {
      path,
      name,
      component: Component(component),
    };
  } else {
    return {
      path,
      component: Component(component),
    };
  }
}

const navRoutes = [
  View('/', 'Index', 'index'),
  // { path: '/', redirect: { name: 'courses' } },

  // Need this route if the app was installed on mobile
  View('/index.html', 'Courses'),
  // { path: '/index.html', redirect: { name: 'courses' } },

  View('/courses', 'Courses', 'courses'),
  // View('', 'Courses', 'courses'),
  {
    path: '/courses', component: Component('Course'),
    children: [
      View('ege-inf', 'EGE', 'ege'),
      View('oge-inf', 'OGE', 'oge'),
      View('delphi', 'Delphi'),
      View('cpp-student', 'CppStudent'),
      View('linux', 'Linux'),
      View('python', 'Python'),
      View('matlab-octave', 'MatlabOctave'),
    ],
  },


  View('/contacts', 'Contacts'),


  View('/faq', 'FAQ', 'faq'),
  { path: '/ru/faq', redirect: '/faq' },
  { path: '/en/faq', redirect: '/faq' },

  // View('/pay', 'Pay', 'pay'),
  View('/price', 'Price', 'price'),
  { path: '/pay', redirect: '/price' },


  View('/baumanka/*', 'Baumanka'),

  // { path: '*', component: Component('404') },
] as RouteConfig[];
// ] as <Array<RouteConfig> > ;

const routerOptions = {
  routes: navRoutes,
  // beforeEach: (to, from, next) => {
  //   const requiresAuth = to.matched.some((record) => record.meta.requiresAuth);
  //   if (requiresAuth) {
  //     next('/login');
  //   } else {
  //     next();
  //   }
  // },
  mode: 'history',
  scrollBehavior: () => ({ x: 0, y: 0 }),
} as RouterOptions;

// export default new Router(routerOptions);
export function createRouter() {
  return new Router(routerOptions);
}
