const express = require('express')
// import path from 'path';
const path = require('path');
// import { createBundleRenderer } from 'vue-server-renderer';
const { createBundleRenderer } = require('vue-server-renderer');
// import express from 'express';

// const root = '/tmp/dist';
const root = '../dist';


// const serverBundle = require('../dist/vue-ssr-server-bundle.json');
// const clientManifest = require('../dist/vue-ssr-client-manifest.json');

const serverBundle = require(`${root}/vue-ssr-server-bundle.json`);
const clientManifest = require(`${root}/vue-ssr-client-manifest.json`);


// console.log(JSON.parse(readFile('vue-ssr-client-manifest.json')));
// const { getWebpackConfigs } = require('vue-cli-plugin-ssr/lib/webpack');
// console.log(getWebpackConfigs);


const template = require('fs').readFileSync(
  path.join(__dirname, './templates/index.html'),
  'utf-8',
);

const renderer = createBundleRenderer(serverBundle, {
  // с этим параметром код сборки будет выполняться в том же контексте, что и серверный процесс
  runInNewContext: false,
  template,
  clientManifest,

  // Это означает, что не будет происходить автоматическая генерация
  // html кода для загрузки ресурсов и прочего, т.к. нам нужна полная
  // гибкость. В шаблоне мы самостоятельно пометим те места, в которые
  // хотим выводить данный код.
  inject: false,
});


// function renderToString(context) {
//   return new Promise((resolve, reject) => {
//     renderer.renderToString(context, (err, html) => {
//       err ? reject(err) : resolve(html);
//     });
//   });
// }

const app = express();

// Do not serve static or root path will give static index.html
// app.use(express.static(root));


app.get('*', (req, res) => {
  // console.log(req);
  const context = { url: req.url };

  // res.sendFile(__dirname+"/index.html");

  // if (req.url === '/') {
  // No need to pass an app here because it is auto-created by
  // executing the bundle. Now our server is decoupled from our Vue app!
  renderer.renderToString(context, (err, html) => {
    res.set('Content-Type', 'text/html');
    if (err) {
      if (err.url) {
        res.redirect(err.url);
      } else if (err.code === 404) {
        res.status(404).end('Not found');
      } else {
        console.log(err);
        res.status(500).end(JSON.stringify(err));
        // res.status(500).end('Internal server error');
      }
    } else {
      res.end(html);
    }
  });
  // } else {
//     res.sendFile(req.url.substring(1), );
//   }
});

const port = 8080;
// const port = 8083;

const server = app.listen(port, () => {
  console.log(`server started at localhost:${port}`);
});


process.on('SIGTERM', () => {
  console.info('SIGTERM signal received.');
  server.close(() => {
    console.log('Http server closed.');
  });
});
