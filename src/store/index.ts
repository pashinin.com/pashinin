/* eslint-disable no-param-reassign */
import Vue from 'vue';
import Vuex from 'vuex';
// import API from 'components/api';
import Repetitor from './modules/repetitor';
import { User, App } from 'commonfe';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    repetitor: Repetitor,
    user: User,
    app: App,
  },
  mutations: {
    changeMenu(state, menu) {
      state.menu = menu;
    },
    setBreadcrumb(state, bc) {
      state.breadcrumb = bc;
    },
    increment(state) {
      state.count += 1;
    },
    selectMenuItem(state, url) {
      state.menu.selected = url;
    },
  },
  state: {
    breadcrumb: [
    ],
    count: 0,
    menu: {
      brand: { title: '' },
      selected: '/',
      items: [],
    },
  },
});
