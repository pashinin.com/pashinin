/* eslint-disable no-param-reassign */
import Vue from 'vue';
import Vuex from 'vuex';
// import Router from 'vue-router';

Vue.use(Vuex);

const mutations = {

};

export default {
  // namespaced: true,
  state: {
    pic: require('@/assets/img/sergey-pashinin-2.jpg'),
    email: 'sergey@pashinin.com',
    phone: '+7 (977) 801-25-41',
    phone2: '+7 (916) 750-22-11',
    skype: 'sergey-pashinin',
    discord: 'Сергей Пашинин#6974',
    prices: {
      1: 1400,
      1.5: 2100,
      2: 2600,
    },
  },
  mutations,
};
