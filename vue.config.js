// https://github.com/ediaos/vue-cli3-ssr-project/blob/master/vue.config.js

const { argv } = require('yargs');
const path = require('path');
const VueSSRServerPlugin = require('vue-server-renderer/server-plugin');
const VueSSRClientPlugin = require('vue-server-renderer/client-plugin');

const ExtractCSSChunksPlugin = require('extract-css-chunks-webpack-plugin');

const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const nodeExternals = require('webpack-node-externals');

// извлечение CSS должно использоваться только в production
// чтобы работал hot-reload на этапе разработки.
const isProduction = process.env.NODE_ENV === 'production'

console.log(process.env.NODE_ENV);


const SSR = argv.ssr;
const isDev = process.env.NODE_ENV && process.env.NODE_ENV.indexOf("dev") > -1;
argv.client = false;
let target = SSR ? 'server' : 'client';

console.log(target);

process.env.VUE_APP_VERSION = require('./package.json').version;

const plugins = [
  new ExtractCSSChunksPlugin({
    // Options similar to the same options in webpackOptions.output
    // all options are optional
    filename: '[name].css',
    chunkFilename: '[id].css',
    // ignoreOrder: false, // Enable to remove warnings about conflicting order
  }),
];
if (argv.anal) {
  plugins.push(new BundleAnalyzerPlugin());
}

// SSR
if (SSR) {
  // Этот плагин преобразует весь результат серверной сборки в один
  // JSON-файл. Имя по умолчанию будет `vue-ssr-server-bundle.json`
  console.log('vue-ssr-server-bundle.json');
  plugins.push(new VueSSRServerPlugin());

  if (isProduction) {
    // plugins.push(new ExtractCSSChunksPlugin({
    //   filename: '[name].css',
    // }));
  }
} else {
  target = 'client';

  // Плагин генерирует `vue-ssr-client-manifest.json` в output-каталоге
  console.log('vue-ssr-client-manifest.json');
  plugins.push(new VueSSRClientPlugin());
}


module.exports = {
  // outputDir: '/tmp/dist',
  // outputDir: 'dist',            // default
  // productionSourceMap: false,

  pwa: {
    // configure the workbox plugin
    themeColor: '#FFFFFF',
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      // swSrc is required in InjectManifest mode.
      // swSrc: `./src/${app}/public/service-worker.js`,
      swSrc: './public/service-worker.js',
      // ...other Workbox options...
      importWorkboxFrom: 'local',
    },
  },

  pages: {
    index: {
      // entry for the page
      // entry: `./src/${app}/main.ts`,
      // entry: `./src/${app}/entry-${target}.ts`,
      entry: `./src/entry-${target}.ts`,
      // entry: [`./src/entry-${target}.ts`],

      // the source template
      template: './src/index.html',

      // output as dist/index.html
      filename: 'index.html',
      // when using title option,
      // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
      // title: 'Index Page ',

      // chunks to include on this page, by default includes
      // extracted common chunks and vendor chunks.
      chunks: [
        'vendor', // Common modules (rare updates)
        'vuetify', // Need it. Otherwise - blank screen.
        // 'commonfe', // Need it. Otherwise - blank screen. It is in 'vendors' now
        // ~index
        'chunk-common',

        'index',
        // 'vendors~index',
      ],
    },

    // when using the entry-only string format,
    // template is inferred to be `public/subpage.html`
    // and falls back to `public/index.html` if not found.
    // Output filename is inferred to be `subpage.html`.
    // baumanka: 'src/baumanka/main.ts',
  },

  // assetsDir: 'static',
  // assetsPublicPath: '/static/',
  css: {
    // if enable sourceMap:  fix ssr load Critical CSS throw replace of undefind
    // sourceMap: !isDev && !SSR,
    sourceMap: !isDev,

    loaderOptions: {
      css: {
        // It doesn't embed CSS but only exports the identifier mappings.
        // Useful when you use css modules for pre-rendering (for example SSR).
        // Docs: https://webpack.js.org/loaders/css-loader/#onlylocals
        onlyLocals: SSR, // (boolean)
        //
        //
        // "onlyLocals" became invalid with new (4.2.0) css-loader
        // https://github.com/webpack-contrib/css-loader/blob/master/CHANGELOG.md#300-2019-06-11
        // requireModuleExtension: true,
        // modules: {
        //   exportOnlyLocals: SSR, // (boolean)
        // },
      },
      postcss: {
        // options here will be passed to postcss-loader
      },
      sass: {
        additionalData: `@import "~@/scss/variables.scss"`,
      },
      scss: {
        // additionalData: `@import "~@/scss/variables.scss";`
      },
    },
  },

  // options: {
  //   attrs: [':data-src']
  // }

  /* to configure <app>/public as a static assets folder */
  configureWebpack: {
    performance: {
      maxEntrypointSize: 400000,
      maxAssetSize: 200000,
    },


    // -------------------
    // For server build:
    // https://ssr.vuejs.org/ru/guide/build-config.html#конфигурация-серверной-части
    // entry: `./src/entry-${target}.ts`,
    // entry: {
    //   app: `./src/entry-${target}.ts`,
    // },
    // target: SSR ? 'node' : 'web',
    target: (SSR) ? 'node' : 'web',


    // For better debugging
    // https://webpack.js.org/configuration/devtool/#devtool
    devtool: 'source-map',

    node: (SSR) ? {
      console: false,
      global: true,
      process: true,
      __filename: 'mock',
      __dirname: 'mock',
      Buffer: true,
      setImmediate: true,
    } : false,

    // Это сообщает что в серверной сборке следует использовать экспорты в стиле Node
    output: {
      libraryTarget: (SSR) ? 'commonjs2' : undefined,
      // libraryTarget: 'commonjs2',
    },
    // https://webpack.js.org/configuration/externals/#function
    // https://github.com/liady/webpack-node-externals
    // Внешние зависимости приложения. Это значительно ускоряет процесс
    // сборки серверной части и уменьшает размер итогового файла сборки.
    externals: (SSR) ? nodeExternals({
      // не выделяйте зависимости, которые должны обрабатываться Webpack.
      // здесь вы можете добавить больше типов файлов, например сырые *.vue файлы
      // нужно также указывать белый список зависимостей изменяющих `global` (например, полифиллы)
      whitelist: [
        /^vuetify/,
        /^commonfe/,
        /^register-service-worker/,
        /\.css$/,
        /\.scss$/,
      ],
      // whitelist: /\.css$/,
    }) : undefined,
    // --------------------

    // Docs for 'ptimization'
    // https://webpack.js.org/configuration/optimization/
    optimization: {
      // Docs:
      // https://webpack.js.org/plugins/split-chunks-plugin/
      splitChunks: (SSR) ? false : {

        // This indicates which chunks will be selected for
        // optimization. When a string is provided, valid values are
        // all, async, and initial. Providing 'all' can be particularly
        // powerful, because it means that chunks can be shared even
        // between async and non-async chunks.
        chunks: 'all',
        // Put everything in node_modules into a file called vendors~main.js
        // chunks: 'async',

        // maxInitialRequests: 3,

        minSize: 10000,

        // Try to split chunks bigger than maxSize into smaller parts
        // maxSize: 250000,

        cacheGroups: {
          vendor: {
            // test: /[\\/]node_modules[\\/](axios|@mdi\/js|vue-meta|core-js|vue|vue-infinite-loading|vuex|date-fns)[\\/]/,
            test: /[\\/]node_modules[\\/](((?!vuetify).)*)[\\/]/, // all except 'vuetify'
            name: 'vendor',
            chunks: 'initial', // all, sync, initial
          },

          vuetify: {
            minSize: 0,
            // minChunks: 2,
            // name: 'vuetify',
            // test: /[\\/]node_modules[\\/]/,
            // test: /[\\/]vuetify[\\/]/,
            test: /[\\/]node_modules[\\/](vuetify.*)[\\/]/,
            // test: /vuetify/,
            name: 'vuetify',
            chunks: 'initial', // all, sync, initial
          },
          // commonfe: {
          //   // name: 'commonfe',
          //   minSize: 0,
          //   test: /[\\/]commonfe[\\/]/,
          //   name: 'commonfe',
          //   chunks: 'all',
          // },

          // font: {
          //   minSize: 0,
          //   test: /[\\/]MaterialIcons-Regular.woff2[\\/]/,
          // },

          default: {
            // Minimum number of chunks that must share a module before splitting
            minChunks: 2,

            priority: -20,

            // If the current chunk contains modules already split out
            // from the main bundle, it will be reused instead of a new
            // one being generated. This can impact the resulting file
            // name of the chunk.
            reuseExistingChunk: true,
          },
        },
      },
    },


    plugins,
  },

  // devServer: process.env.NODE_ENV !== 'development' ? undefined : {
  devServer: {
    // compress: true,

    // По умолчанию, webpack-dev-server раздает ресурсы из оперативной
    // памяти, не записывая их на диск. В таком случае мы сталкиваемся с
    // проблемой, что в клиентском манифесте
    // (vue-ssr-client-manifest.json), который размещен на диске, будут
    // указаны неактуальные ресурсы, т.к. он не будет обновлен. Чтобы
    // обойти это, мы говорим дев-серверу записывать изменения на диск,
    // в таком случае клиентский манифест будет обновлен, и подтянутся
    // нужные ресурсы.
    //
    // На самом деле, в будущем хочется избавиться от этого. Одно из
    // решений — в дев. режиме в server.js подключать манифест не из
    // каталога /dist, а с урла дев-сервера. Но в таком случае это
    // становится асинхронной операцией. Буду рад красивому варианту
    // решения проблемы в комментариях.
    // writeToDisk: !SSR || argv.client,
    // writeToDisk: true,

    port: (SSR) ? 3000 : undefined,
    contentBase: path.join(__dirname, 'dist'),
    // SSR ? path.join(__dirname, 'dist') : undefined,

    watchOptions: {
      // ignored: [ignoredFiles(paths.appSrc),/\/\.#.+$/]
      ignored: [/\/\.#.+$/]
    },


    // contentBase: [
    //   // path.join(__dirname, `src/${app}/public`),
    //   // path.join(__dirname, `src/${app}/assets`),
    //   path.join(__dirname, 'src/public'),
    //   path.join(__dirname, 'src/assets'),
    // ],
  },

  // tweak internal webpack configuration.
  // see https://github.com/vuejs/vue-cli/blob/dev/docs/webpack.md
  chainWebpack: (config) => {
    // fix ssr hot update bug
    if (SSR) {
      config.plugins.delete('hmr');
    }

    config.module
      .rule('vue')
      .use('vue-loader')
      .tap(options => {
        if (SSR) {
          options.cacheIdentifier += '-server';
          options.cacheDirectory += '-server';
        }
        options.optimizeSSR = SSR;
        return options;
      });

    if (SSR) {
      // Remove extract-css-loader
      const langs = ['css', 'postcss', 'scss', 'sass', 'less', 'stylus'];
      const types = ['vue-modules', 'vue', 'normal-modules', 'normal'];
      for (const lang of langs) {
        for (const type of types) {
          const rule = config.module.rule(lang).oneOf(type);
          rule.uses.delete('extract-css-loader');
          // Critical CSS
          rule
            .use('vue-style')
            .loader('vue-style-loader') // css-loader/locals      vue-style-loader (working)
            .before('css-loader');
        }
      }
      config.plugins.delete('extract-css');
    }

    // config.optimization.delete('splitChunks');

    // If you wish to remove the standard entry point
    // config.entryPoints.delete('app');

    // then add your own
    // config.entry('pashinin').add('./src/pashinin/main.ts').end();
    // .entry('baumanka').add('./src/baumanka/index.js').end();
  },
};
